# DemoShopTestingApp

## Short Description

This project tests the following features of the Demo Shop App:

- Login
- Products Page
- Search
- Add To Wishlist
- Add To Cart
- Checkout


## Demo Shop Test Automation Framework

The automation tests are grouped in feature test classes based on the Demo Shop App feature that will be tested.
All page elements and functions are located in Page classes. 

For example, all automation tests for the Shopping Cart
are grouped in the 'CartTests' class, and all shopping cart element and related functions are
located in the 'CartPage' class.

Each test feature class contains a function annotated with @BeforeMethod which ensures that the app
is in the correct state before each test run( home page opened, cart is empty, wishlist is empty and there is no user logged in)  
## This is the final project for Dana, within the FastTrackIt Test Automation course.

### Software engineer: _Dana Pop_

### Tech stack used:
    - Java17
    - Maven
    - Selenide Framwork
    - PageObject Models

### How to run the tests

`git clone https://gitlab.com/danaioana.pop/demoshop.git`

Execute the following commands to:

#### Execute all tests
- `mvn test`
#### Generate Report
- `mvn allure:report`
#### Open and present report
- `mvn allure:serve`

#### Page Objects
    - Footer
    - Header
    - Modal
    - Products
    - Cart page
    - Checkout Page
    - Main Page
    - WishlistPage
    - Account

### Test Feature Classes
    - CartTests
    - CheckoutTests
    - LoginTests
    - ProductTests
    - StaticApplicationStateTests
    - WishlistTests