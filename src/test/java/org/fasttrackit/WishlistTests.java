package org.fasttrackit;
import io.qameta.allure.Feature;
import org.fasttrackit.Pages.MainPage;
import org.fasttrackit.Pages.WishlistPage;
import org.fasttrackit.body.Header;
import org.fasttrackit.body.Modal;
import org.fasttrackit.body.Products;
import org.fasttrackit.dataprovider.ProductDataProvider;
import org.fasttrackit.products.Product;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import static com.codeborne.selenide.Selenide.sleep;
import static org.testng.Assert.assertTrue;

@Feature("Wishlist Tests")
public class WishlistTests extends TestConfiguration {

    MainPage homePage;
    WishlistPage wishlistPage = new WishlistPage();
    Products products = new Products();
    Header header = new Header();
    Modal modal = new Modal();

    @BeforeMethod
    public void setup(){
        homePage = new MainPage();
        homePage.resetAppState();
    }

    @Test
    public void guest_user_clicks_on_wishlist_button_and_page_wishlist_is_displayed(){
        homePage.clickOnTheWishListButton();
        sleep(5000);
        assertTrue(wishlistPage.validateThatWishlistPageIsDisplayed(),"wishlist not displayed!");
    }

    @Test(dataProvider = "productsDataProvider",dataProviderClass = ProductDataProvider.class)
    public void user_can_add_all_products_to_wishlist(Product p){

        p.addToFavorite();
        homePage.clickOnTheWishListButton();
        assertTrue(wishlistPage.allProductInWishlistPage(),"Products are not displayed");

    }

    @Test(testName = "Guest user can add multiple products to wishlist")
    public void guest_user_add_products_to_wishlist_and_verify_wishlist_page(){
        String productTitle = "Mouse";
        products.clickOnTheSearchBar();
        products.typeInSearchBar(productTitle);
        sleep(1000);
        products.clickOnSearchButton();
        sleep(1000);

        products.addAllSearchProductsToWishlist();
        header.clickOnTheWishListButton();
        assertTrue(wishlistPage.verifyWishListContentIsCorrect(productTitle), "WishList content incorrect!");
    }

    @Test(testName = "Logged user can add multiple products to wishlist")
    public void logged_user_add_products_to_wishlist_and_verify_wishlist_page(){
        homePage.clickOnTheLoginButton();
        modal.typeInUserNameField("dino");
        modal.typeInPasswordField("choochoo");
        modal.clickOnTheLoginButton();
        String productTitle = "Mouse";
        products.clickOnTheSearchBar();
        products.typeInSearchBar(productTitle);
        sleep(1000);
        products.clickOnSearchButton();
        sleep(1000);

        products.addAllSearchProductsToWishlist();
        header.clickOnTheWishListButton();
        assertTrue(wishlistPage.verifyWishListContentIsCorrect(productTitle), "WishList content incorrect!");
    }
}
