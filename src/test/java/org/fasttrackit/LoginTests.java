package org.fasttrackit;
import io.qameta.allure.Feature;
import org.fasttrackit.Pages.MainPage;
import org.fasttrackit.body.Footer;
import org.fasttrackit.body.Header;
import org.fasttrackit.body.Modal;
import org.fasttrackit.dataprovider.AccountDataProvider;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import static com.codeborne.selenide.Selenide.sleep;
import static org.testng.Assert.*;

@Feature("Login Tests")
public class LoginTests extends TestConfiguration {
    MainPage homePage;


    @BeforeMethod
    public void setup(){
        homePage = new MainPage();
        homePage.resetAppState();
    }

    @Test(testName = "Login modal components are present and modal can be closed")
    public void modal_components_are_displayed_modal_can_be_closed_test(){

        homePage.clickOnTheLoginButton();
        boolean modalIsDisplayed = homePage.validateModalIsDisplayed();
        assertTrue(modalIsDisplayed,"Expected modal is  displayed");
        Modal modal = new Modal();

        assertEquals(modal.getModalTitle(),"Login","Expected modal title to be login");
        assertTrue(modal.validateCloseButtonIsDisplayed(),"Expected close button to be  displayed");
        assertTrue(modal.userNameFieldIsdisplayed(),"Expected username field to be  displayed");
        assertTrue(modal.passwordFieldIsDispyed(),"Expected password field to be  displayed");
        assertTrue(modal.loginBottonIsDisplayed(),"Expected login button to be  displayed");
        modal.clickOnCloseButton();

        sleep(500);
        assertFalse(homePage.validateModalIsDisplayed(),"Expected modal to be closed");

    }

    @Test(testName ="Login Test with valid User",
            dataProvider = "AccountDataProvider", dataProviderClass = AccountDataProvider.class )
    public void user_can_login_on_demo_app(Account account){

        homePage.clickOnTheLoginButton();
        Modal modal = new Modal();
        modal.typeInUserNameField(account.getUsername());
        modal.typeInPasswordField(account.getPassword());
        modal.clickOnTheLoginButton();
        assertTrue(homePage.validateModalIsNotDisplayed(),"Modal was not closed!");
        Header header = new Header(account.getUsername());
        assertEquals(header.getGreetingsMessage(), account.getGreetingMsg(),"Username not found in greetings message!");

    }
    @Test(testName = "User cannot login with locked account")
    public void user_cannot_login_with_locked_account(){
        Account lockedAccount = new Account("locked","choochoo");
        String expectedErrorMsg = "The user has been locked out.";
        homePage.clickOnTheLoginButton();
        Modal modal = new Modal();
        modal.typeInUserNameField(lockedAccount.getUsername());
        modal.typeInPasswordField(lockedAccount.getPassword());
        modal.clickOnTheLoginButton();
        assertTrue(homePage.validateModalIsDisplayed(),"Expected modal to remain opened ");

        assertEquals(modal.getModalMessage(),expectedErrorMsg, "The message was not as expected ");

    }

    @Test(testName = "User cannot login without password", description = "this test is responsible for testing that the user cant login without password ")
    public void user_cannot_login_without_password() {
        homePage.clickOnTheLoginButton();
        Modal modal = new Modal();
        String user = "beetle";
        String expectedErrorMsg = "Please fill in the password!";
        modal.typeInUserNameField(user);
        modal.clickOnTheLoginButton();
        assertTrue(homePage.validateModalIsDisplayed(), "Expected modal to remain on page");
        assertEquals(modal.getModalMessage(),expectedErrorMsg, "The message was not as expected");

    }

    @Test(testName = "User cannot login without username")
    public void user_cannot_login_without_username() {
        homePage.clickOnTheLoginButton();
        Modal modal = new Modal();
        String password = "choochoo";
        String expectedErrorMsg = "Please fill in the username!";
        modal.typeInPasswordField(password);
        modal.clickOnTheLoginButton();
        assertTrue(homePage.validateModalIsDisplayed(), "Expected modal to remain on page");
        assertEquals(modal.getModalMessage(),expectedErrorMsg, "The message was not as expected");

    }
}
