package org.fasttrackit;
import io.qameta.allure.Feature;
import org.fasttrackit.Pages.CartPage;
import org.fasttrackit.Pages.CheckoutPage;
import org.fasttrackit.Pages.MainPage;
import org.fasttrackit.body.Header;
import org.fasttrackit.body.Products;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import static com.codeborne.selenide.Selenide.sleep;
import static org.testng.Assert.assertEquals;

@Feature("Checkout tests")
public class CheckoutTests extends TestConfiguration {

    MainPage homePage;
    Products products = new Products();
    Header header = new Header();
    CartPage cartPage = new CartPage();
    CheckoutPage checkout = new CheckoutPage();

    @BeforeMethod
    public void setup(){
        homePage = new MainPage();
        homePage.resetAppState();
    }

    @Test(testName = "Guest user can add to cart and complete checkout process")
    public void guest_user_can_complete_checkout(){

        String product = "Licensed Steel Gloves";
        products.clickOnTheSearchBar();
        products.typeInSearchBar(product);
        products.clickOnSearchButton();
        products.clickOnAddProductButton();

        header.clickOnShoppingCartIcon();
        cartPage.clickOnTheCheckoutButton();
        checkout.insertFirstName("Dana");
        sleep(1000);
        checkout.insertLastName("Pop");
        sleep(1000);
        checkout.insertAddress("Cluj Romania");
        sleep(1000);
        checkout.clickContinueCheckout();
        checkout.clickOnCompleteCheckout();
        assertEquals(checkout.getCheckoutMessage(),"Thank you for your order!", "Checkout was not successful!");
    }

    @Test(testName = "Verify contact information mandatory fields")
    public void verify_contact_information_is_required_for_checkout(){
        String product = "Licensed Steel Gloves";
        products.clickOnTheSearchBar();
        products.typeInSearchBar(product);
        products.clickOnSearchButton();
        products.clickOnAddProductButton();

        header.clickOnShoppingCartIcon();
        cartPage.clickOnTheCheckoutButton();
        checkout.clickContinueCheckout();
        assertEquals(checkout.verifyFirstNameIsRequired(),"First Name is required","First name is not mandatory");
        checkout.insertFirstName("Dana");
        checkout.clickContinueCheckout();
        assertEquals(checkout.verifyLastNameIsRequired(),"Last Name is required","Last name is not mandatory");
        checkout.insertLastName("Pop");
        checkout.clickContinueCheckout();
        assertEquals(checkout.verifyAddressIsRequired(),"Address is required","Address is not mandatory");
    }
}
