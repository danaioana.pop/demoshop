package org.fasttrackit;
import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Feature;
import org.fasttrackit.Pages.MainPage;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import static org.testng.Assert.*;

@Feature("Static Tests")
public class StaticApplicationStateTest extends TestConfiguration {
    MainPage homePage;

    @BeforeMethod
    public void setup(){
        homePage = new MainPage();
        homePage.resetAppState();
    }

    @Test
    public void verifyDemoShopAppTitle() {

        String appTitle = homePage.getPageTitle();
        assertEquals(appTitle,"Demo shop","Aplication title is expected to be Demo shop");

    }
    @Test
    public void verifyDemoShopFooterBuildDate(){

        String footerDetails = homePage.getFooter().getDetails();
       assertEquals(footerDetails,"Demo Shop | build date 2021-05-21 14:04:30 GTBDT","Expected footer to be:Demo Shop | build date 2021-05-21 14:04:30 GTBDT");
    }

    @Test
    public void verifyDemoShopFooterContainsQuestionIcon() {

        SelenideElement questionIcon = homePage.getFooter().getQuestionIcon();
        assertTrue(questionIcon.exists(),"Expected Question Icon to exist on Page");
        assertTrue(questionIcon.isDisplayed(),"Expected Question Icon to be displayed");
    }

    @Test
    public void verifyDemoShopFooterContainsResetIcon(){
        SelenideElement resetIconTitle = homePage.getFooter().getResetIconTitle();
        assertTrue(resetIconTitle.exists(),"Expected Reset Icon to exist on Page");
        assertTrue(resetIconTitle.isDisplayed(),"Expected Reset Icon to be displayed");
    }

    @Test
    public void verifyDemoShopHeaderContainsShoppingCartIcon(){
        SelenideElement headerShoppingCartIcon = homePage.getHeader().getShopingCartIcon();
        assertTrue(headerShoppingCartIcon.exists(),"Expected Shoping Cart Icon to exist on Page");
        assertTrue(headerShoppingCartIcon.isDisplayed(),"Expected Shoping Cart Icon to be displayed");
    }

    @Test
    public void verifyDemoShopHeaderContainsWishlistIconUrl(){
        SelenideElement headerWishlistIconUrl = homePage.getHeader().getWishlistIconUrl();
        assertTrue(headerWishlistIconUrl.exists(),"Expected Wishlist Icon to exist on Page");
        assertTrue(headerWishlistIconUrl.isDisplayed(),"Expected Wishlist Icon to be displayed");
    }

    @Test
    public void verifyDemoShopHeaderContainsSignInButton(){
        SelenideElement headerSignInButton = homePage.getHeader().getSignInButton();
        assertTrue(headerSignInButton.exists(),"Expected Sign In Button to exist on Page");
        assertTrue(headerSignInButton.isDisplayed(),"Expected Sign In Button to be displayed");
    }


}
