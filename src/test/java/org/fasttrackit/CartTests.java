package org.fasttrackit;
import io.qameta.allure.Feature;
import org.fasttrackit.Pages.CartPage;
import org.fasttrackit.Pages.MainPage;
import org.fasttrackit.body.Header;
import org.fasttrackit.body.Modal;
import org.fasttrackit.body.Products;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import static com.codeborne.selenide.Selenide.sleep;
import static org.testng.Assert.assertTrue;

@Feature("Cart Tests")
public class CartTests extends TestConfiguration {

    MainPage homePage;
    Header header = new Header();
    Products products =new Products();
    CartPage cartPage = new CartPage();
    Modal modal = new Modal();

    @BeforeMethod
    public void setup(){
        homePage = new MainPage();
        homePage.resetAppState();
    }

    @Test(testName = "Guest user can add product to cart")
    public void  guest_user_can_add_product_in_cart(){

        String productTitle = "Awesome Granite Chips";
        products.clickOnTheSearchBar();
        products.typeInSearchBar(productTitle);
        products.clickOnSearchButton();
        products.clickOnAddProductButton();
        header.clickOnShoppingCartIcon();

        assertTrue(cartPage.verifyProductsAreFoundInCart(productTitle),"Cart content is incorrect!");

    }

    @Test(testName = "Guest user can add more products in cart")
    public void guest_user_can_add_more_products_in_cart(){

        String productTitle = "Awesome";
        products.clickOnTheSearchBar();
        products.typeInSearchBar(productTitle);
        sleep(1000);
        products.clickOnSearchButton();
        products.addAllSearchProductsToCart();
        header.clickOnShoppingCartIcon();
        assertTrue(cartPage.verifyProductsAreFoundInCart(productTitle),"Cart content is incorrect!");

    }

    @Test(testName = "Logged in user with bugs can add product to cart", priority = 0)
    public void logged_user_with_bugs_can_add_product_in_cart(){

        homePage.clickOnTheLoginButton();
        modal.typeInUserNameField("beetle");
        modal.typeInPasswordField("choochoo");
        modal.clickOnTheLoginButton();

        String productTitle = "Awesome Granite Chips";
        products.clickOnTheSearchBar();
        products.typeInSearchBar(productTitle);
        products.clickOnSearchButton();
        products.clickOnAddProductButton();
        sleep(500);
        header.clickOnShoppingCartIcon();

        sleep(2000);
        assertTrue(cartPage.verifyProductsAreFoundInCart(productTitle),"Cart content is incorrect!");


    }
    @Test(testName = "Normal user can add product to cart")
    public void logged_normal_user_can_add_product_in_cart(){

        homePage.clickOnTheLoginButton();
        modal.typeInUserNameField("dino");
        modal.typeInPasswordField("choochoo");
        modal.clickOnTheLoginButton();
        String productTitle = "Awesome Granite Chips";
        products.clickOnTheSearchBar();
        products.typeInSearchBar(productTitle);
        products.clickOnSearchButton();
        products.clickOnAddProductButton();
        sleep(500);
        header.clickOnShoppingCartIcon();

        sleep(2000);
        assertTrue(cartPage.verifyProductsAreFoundInCart(productTitle),"Cart content is incorrect!");

    }
    @Test(testName = "Normal user can add multiple products to cart")
    public void logged_normal_user_can_add_more_products_in_cart(){

        homePage.clickOnTheLoginButton();
        modal.typeInUserNameField("dino");
        modal.typeInPasswordField("choochoo");
        modal.clickOnTheLoginButton();
        String productTitle = "Soft";
        products.clickOnTheSearchBar();
        products.typeInSearchBar(productTitle);
        products.clickOnSearchButton();
        products.addAllSearchProductsToCart();
        sleep(500);
        header.clickOnShoppingCartIcon();

        sleep(2000);
        assertTrue(cartPage.verifyProductsAreFoundInCart(productTitle),"Cart content is incorrect!");

    }
}
