package org.fasttrackit;
import io.qameta.allure.Feature;
import org.fasttrackit.Pages.MainPage;
import org.fasttrackit.body.Modal;
import org.fasttrackit.body.Products;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import static com.codeborne.selenide.Selenide.sleep;
import static org.testng.Assert.*;

@Feature("Product Tests")
public class ProductTest extends TestConfiguration {

    MainPage homePage;
    Products products = new Products();
    Modal modal = new Modal();

    @BeforeMethod
    public void setup(){
        homePage = new MainPage();
        homePage.resetAppState();
    }



    @Test(testName = "Search bar is present on the Products page")
    public void  verify_search_bar_is_displayed_on_products_page() {

        assertTrue(products.getSearchBar(), "Search Bar is not displayed");
    }

    @Test(testName = "Search button is present on the Products page")
    public void Verify_search_button_is_displayed(){

        assertTrue(products.getSearchButton(),"Search Button is not Displayed");

    }

    @Test(testName = "Guest user can search for products using Search Bar")
    public void guest_user_can_search_product_with_search_bar(){

        String productTitle = "Awesome Granite Chips";
        products.clickOnTheSearchBar();
        products.typeInSearchBar(productTitle);
        sleep(1000);
        products.clickOnSearchButton();
        sleep(1000);

        assertEquals(products.getSearchResultsProductTitle(), productTitle, "Search result is not as expected");

    }

    @Test(testName = "Gust user can search and have multiple results")
    public void guest_user_verify_search_with_multiple_results(){

        String productTitle = "Awesome";
        products.clickOnTheSearchBar();
        products.typeInSearchBar(productTitle);
        sleep(1000);
        products.clickOnSearchButton();
        sleep(1000);
        assertTrue(products.verifySearchResultsMatchTheSearchTerm(productTitle));

    }

    @Test(testName = "Logged in user can search for products")
    public void verify_search_with_logged_user(){
        String userName = "beetle";
        String password = "choochoo";
        String productTitle = "Awesome Granite Chips";

        homePage.clickOnTheLoginButton();
        modal.typeInUserNameField(userName);
        modal.typeInPasswordField(password);
        modal.clickOnTheLoginButton();

        products.clickOnTheSearchBar();
        products.typeInSearchBar(productTitle);
        sleep(1000);
        products.clickOnSearchButton();
        sleep(1000);


        assertTrue(products.getSearchResultsProductTitle().equals(productTitle),"Search result is not as expected");

    }

    @Test(testName = "Logged in user can search and have multiple results")
    public void verify_search_with_multiple_results_with_logged_user(){
        String userName = "beetle";
        String password = "choochoo";

        homePage.clickOnTheLoginButton();
        Modal modal = new Modal();
        modal.typeInUserNameField(userName);
        modal.typeInPasswordField(password);
        modal.clickOnTheLoginButton();
        String productTitle = "Awesome";
        products.clickOnTheSearchBar();
        products.typeInSearchBar(productTitle);
        sleep(1000);
        products.clickOnSearchButton();
        sleep(1000);
        assertTrue(products.verifySearchResultsMatchTheSearchTerm(productTitle));

    }

    @Test(testName = "Guest user can sort by price low to high")
    public void guest_user_can_sort_by_price_low_to_high(){

        products.clickOnSortDropDownButton();
        products.selectSortOption("Sort by price (low to high)");
        assertTrue(products.verifyProductsAreOrderedByPriceLowToHigh(),"Items not sorted correctly!");

    }

    @Test(testName = "Guest user can sort by price high to low")
    public void guest_user_can_sort_by_price_high_to_low(){

        products.clickOnSortDropDownButton();
        products.selectSortOption("Sort by price (high to low)");
        assertTrue(products.verifyProductsAreOrderedByPriceHighToLow(),"Items not sorted correctly!");

    }
    @Test
    public void logged_user_with_bugs_can_sort_by_price_low_to_high(){

        homePage.clickOnTheLoginButton();
        Modal modal = new Modal();
        modal.typeInUserNameField("beetle");
        modal.typeInPasswordField("choochoo");
        modal.clickOnTheLoginButton();
        products.clickOnSortDropDownButton();
        products.selectSortOption("Sort by price (low to high)");
        sleep(5000);
        assertTrue(products.verifyProductsAreOrderedByPriceLowToHigh(),"Items not sorted correctly!");

    }
    @Test
    public void logged_user_with_bugs_can_sort_by_price_high_to_low() {
        String userName = "beetle";
        String password = "choochoo";
        homePage.clickOnTheLoginButton();
        Modal modal = new Modal();
        modal.typeInUserNameField(userName);
        modal.typeInPasswordField(password);
        modal.clickOnTheLoginButton();
        products.clickOnSortDropDownButton();
        products.clickOnSortDropDownButton();
        products.selectSortOption("Sort by price (high to low)");
        assertTrue(products.verifyProductsAreOrderedByPriceHighToLow(), "Items not sorted correctly!");

    }
    @Test
    public void normal_user_logged_can_sort_by_price_low_to_high(){

        homePage.clickOnTheLoginButton();
        Modal modal = new Modal();
        modal.typeInUserNameField("dino");
        modal.typeInPasswordField("choochoo");
        modal.clickOnTheLoginButton();
        products.clickOnSortDropDownButton();
        products.selectSortOption("Sort by price (low to high)");
        sleep(5000);
        assertTrue(products.verifyProductsAreOrderedByPriceLowToHigh(),"Items not sorted correctly!");

    }
    @Test
    public void  normal_user_logged_can_sort_by_price_high_to_low() {

        homePage.clickOnTheLoginButton();
        Modal modal = new Modal();
        modal.typeInUserNameField("turtle");
        modal.typeInPasswordField("choochoo");
        modal.clickOnTheLoginButton();
        products.clickOnSortDropDownButton();
        products.clickOnSortDropDownButton();
        products.selectSortOption("Sort by price (high to low)");
        assertTrue(products.verifyProductsAreOrderedByPriceHighToLow(), "Items not sorted correctly!");

    }
}
