package org.fasttrackit.head;

import com.codeborne.selenide.Selenide;
import org.fasttrackit.DemoShopApp;

public class Head {
private String title = Selenide.title();

    public  Head()
 {
         this.title = DemoShopApp.Demo_Shop_Title;
    }

    public String getTitle() {
        return title;
    }
}
