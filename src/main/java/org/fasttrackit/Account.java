package org.fasttrackit;

public class Account {

    private final String username;
    private final String password;
    private final String greetingMsg;


    public Account(String username, String password) {
        this.username = username;
        this.password = password;
        this.greetingMsg ="Hi "+ this.username + "!";
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getGreetingMsg() {
        return greetingMsg;
    }

    @Override
    public String toString() {
        return this.username;
    }
}
