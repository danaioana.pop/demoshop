package org.fasttrackit.Pages;

import com.codeborne.selenide.SelenideElement;

import java.util.List;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

public class CartPage {

    private final SelenideElement  cartProductTitle = $("[id*=title_link]");
    private final SelenideElement checkoutButton = $(".btn-success");


    public boolean verifyProductsAreFoundInCart(String expectedTitle){
        List<SelenideElement> productTitles = $$("[id*=title_link]");
        Boolean isCartContentCorrect = true;

        if(productTitles.isEmpty()){
            isCartContentCorrect = false;
            System.out.println("The cart is empty!");
        }

        for(SelenideElement productTitle : productTitles){
            if(!productTitle.text().contains(expectedTitle)){
                System.out.println("Title: " + productTitle.text() + " should not be in cart!");
                isCartContentCorrect = false;
                break;
            }
        }
        return isCartContentCorrect;
    }

    public void clickOnTheCheckoutButton() {
        this.checkoutButton.click();

    }
}
