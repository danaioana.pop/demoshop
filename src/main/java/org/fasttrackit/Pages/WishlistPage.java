package org.fasttrackit.Pages;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;

import java.util.List;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

public class WishlistPage extends MainPage {
    private final SelenideElement pageSubtitle = $(".text-muted");
    private final ElementsCollection allProductInWishlistPage= $$(".col .card");


    public boolean validateThatWishlistPageIsDisplayed(){
      return this.pageSubtitle.exists() && this.pageSubtitle.isDisplayed();

      }
    public boolean productInWishlistAreDisplayed(){
        for (SelenideElement product : this.allProductInWishlistPage) {
            if(!product.isDisplayed()){
                return false;
            }
        }
        return true;
    }
      public boolean allProductInWishlistPage(){
        return !this.allProductInWishlistPage.isEmpty();

      }

    public Boolean verifyWishListContentIsCorrect(String expectedTitle) {
        List<SelenideElement> productTitles = $$(".card .card-link");
        Boolean isWishListContentCorrect = true;

        if(productTitles.isEmpty()){
            isWishListContentCorrect = false;
            System.out.println("The Wishlist page is empty!");
        }

        for(SelenideElement title : productTitles){
            if(!title.text().contains(expectedTitle)){
                isWishListContentCorrect = false;
                System.out.println("Item " + title.text() + " should not be in the WishList!");
                break;
            }
        }
        return isWishListContentCorrect;
    }
}




