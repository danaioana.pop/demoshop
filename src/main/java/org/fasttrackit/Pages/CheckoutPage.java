package org.fasttrackit.Pages;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class CheckoutPage {
    private final SelenideElement firstNameField = $("[data-test='firstName']");
    private final SelenideElement lastNameField = $("[data-test='lastName']");
    private final SelenideElement addressField = $("[data-test='address']");
    private final SelenideElement continueCheckOutButton  = $(".btn-success");
    private final SelenideElement completeCheckoutButton  = $("[href='#/checkout-complete']");
    private final SelenideElement orderCompleteMsg  = $(".container .text-center");
    private final SelenideElement checkOutErrorMsg = $("[data-test='error']");



    public void insertFirstName(String firstName) {
        this.firstNameField.click();
        this.firstNameField.sendKeys(firstName);
    }


    public void insertLastName(String lastName) {
        this.lastNameField.click();
        this.lastNameField.sendKeys(lastName);
    }

    public void insertAddress(String address) {
        this.addressField.click();
        this.addressField.sendKeys(address);
    }

    public void clickContinueCheckout() {
        this.continueCheckOutButton.click();
    }

    public void clickOnCompleteCheckout() {
        this.completeCheckoutButton.click();
    }

    public String getCheckoutMessage() {
         return this.orderCompleteMsg.text();
    }

    public String verifyFirstNameIsRequired() {
        return  this.checkOutErrorMsg.text();
    }

    public String verifyLastNameIsRequired() {
        return  this.checkOutErrorMsg.text();
    }

    public String verifyAddressIsRequired() {
        return  this.checkOutErrorMsg.text();
    }
}
