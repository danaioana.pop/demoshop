package org.fasttrackit.Pages;

import com.codeborne.selenide.SelenideElement;
import org.fasttrackit.body.Footer;
import org.fasttrackit.body.Header;
import org.fasttrackit.body.Modal;
import org.fasttrackit.body.Products;
import org.fasttrackit.head.Head;
;import static com.codeborne.selenide.Selenide.$;

public class MainPage extends Page{
    private final Head head;
    private final Footer footer;
    private  Header header;
    private final Products products;


    private final SelenideElement modal = $(".modal-dialog");



    public MainPage() {

       this.head = new Head();
        System.out.println("Constructing Footer");
       this.footer = new Footer();
        System.out.println("Constructing Header");
       this.header = new Header();
       this.products = new Products();

    }
    public void returnToHomePage() {
        this.header.getHomePageIconUrl().click();
    }

    public void setHeader(Header header) {
        this.header = header;
    }

    public Footer getFooter() {
        return footer;
    }

    public Header getHeader() {
        return header;
    }

    public String getPageTitle() {
        String expectedTitle = "Domo shop";
        System.out.println("1.Verify that tiltle is: " + head.getTitle());
        System.out.println("Expected Title: "+ expectedTitle);
        return  head.getTitle();
    }

    public void validateThatFooterContainsAllElements() {
     String details = footer.getDetails();
        System.out.println("......................");
        System.out.println("1. Verify footer details are" + footer.getDetails());
        System.out.println("2. Verify footer has Question Icon: " + footer.getQuestionIcon());
        System.out.println("2. Verify footer has Reset Icon state: " + footer.getResetIconTitle());
        System.out.println("......................");

    }

    public void validateThatHeaderContainsAllElements() {

        System.out.println("1. Verify that logo url is /: " + header.getHomePageIconUrl());
        System.out.println("2. Verify that shoping cart url is /: " + header.getShopingCartIcon());
        System.out.println("3. Verify that wishlist url is /: " + header.getWishlistIconUrl());
        System.out.println("4. Verify that welcome message is /: " + header.getGreetingsMessage());
        System.out.println("5. Verify that sing in is /: " + header.getSignInButton());

    }

    public void clickOnTheLoginButton() {

        this.header.clickOnTheLoginButton();
        Modal modal = new Modal();
    }
    public boolean validateModalIsDisplayed(){
        System.out.println("Verify that the  modal is  displayed on page. ");
        return this.modal.exists() && this.modal.isDisplayed();
    }
    public boolean validateModalIsNotDisplayed(){

        System.out.println("1. Verify that modal is not on page");
        return modal.exists();
    }

    public void clickOnTheWishListButton() {
        System.out.println("......................");
        this.header.clickOnTheWishListButton();
        Modal modal = new Modal();

    }
    public void clickOnTheLogoButton(){
        this.header.homePageIconUrl();

    }

    public void logUserOut() {
        this.header.clickOnTheLogOutButton();
    }

    public void resetAppState(){
        this.footer.getResetIconTitle().click();
    }

}
