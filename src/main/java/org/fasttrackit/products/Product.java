package org.fasttrackit.products;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class Product {
    private final SelenideElement productLink ;
    private final SelenideElement card;
    private final SelenideElement addToBasketButton;
    private final SelenideElement addToFavorite;


    public Product(String productID) {
        this.productLink = $(String.format(".card-body [href='#/product/%s']",productID));
        this.card = productLink.parent().parent();
        this.addToBasketButton = card.$(".fa-cart-plus");
        this.addToFavorite = card.$(".fa-heart");

    }

    public void clickOnProduct(){
        this.productLink.click();
    }

    public void addProductToBasket(){
        this.addToBasketButton.click();
    }

    public void addToFavorite(){
        this.addToFavorite.click();
    }

    @Override
    public String toString() {
        return this.productLink.text();
    }


}
