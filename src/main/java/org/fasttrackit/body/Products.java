package org.fasttrackit.body;

import com.codeborne.selenide.SelenideElement;
import com.google.common.collect.Ordering;

import java.util.ArrayList;
import java.util.List;

import static com.codeborne.selenide.Selenide.*;

public class Products {

    private final SelenideElement searchBar = $("#input-search");
    private final SelenideElement searchButton = $(".col-md-auto button");
    private final SelenideElement productTitles = $(".col .card .card-link");
    private final SelenideElement addToCartButton = $(".text-muted button [data-icon='cart-plus']");
    private final  SelenideElement sortDropDownButton = $(".sort-products-select");

    public boolean getSearchBar() {
        return searchBar.isDisplayed();

    }

    public void clickOnTheSearchBar() {
        this.searchBar.click();
    }

    public void typeInSearchBar(String product) {
        this.searchBar.clear();
        this.searchBar.sendKeys(product);
    }

    public boolean getSearchButton() {
        return searchButton.isDisplayed();
    }
    public void clickOnSearchButton(){
        this.searchButton.click();
    }



    public String getSearchResultsProductTitle(){
        return productTitles.text();
    }

    public boolean verifySearchResultsMatchTheSearchTerm(String expected){
        List<SelenideElement> productTitles = $$(".col .card .card-link");
        for(SelenideElement title : productTitles){
            if(title.text().contains(expected)){
                return true;
            }
        }
        return false;
    }

    public void clickOnAddProductButton(){
        this.addToCartButton.click();
    }

    public void addAllSearchProductsToCart(){
        List<SelenideElement> addToCartButtons = $$(".text-muted button [data-icon='cart-plus']");
        for(SelenideElement button : addToCartButtons) {
            button.click();
            sleep(1000);
        }
    }

    public void clickOnSortDropDownButton(){
        this.sortDropDownButton.click();
    }

    public void selectSortOption(String option){
        List<SelenideElement> sortOptions = $$(".sort-products-select option");
        for(SelenideElement value : sortOptions){
            if(value.text().equals(option)){
                value.click();
            }
        }
    }

    public Boolean verifyProductsAreOrderedByPriceLowToHigh(){
        List<SelenideElement> prices = $$(".card-footer p:nth-child(1)");
        List<String> strings = new ArrayList<>();

        for(SelenideElement price : prices){
            strings.add(price.text().replace("$",""));
        }

        List<Double> doublePrices = new ArrayList<>();
        for(String string : strings){
            doublePrices.add(Double.parseDouble(string));
        }
        System.out.println(doublePrices);
        if(Ordering.natural().isOrdered(doublePrices)){
            return true;
        }
        return false;
    }

    public Boolean verifyProductsAreOrderedByPriceHighToLow(){
        List<SelenideElement> prices = $$(".card-footer p:nth-child(1)");
        List<String> strings = new ArrayList<>();

        for(SelenideElement price : prices){
            strings.add(price.text().replace("$",""));
        }

        List<Double> doublePrices = new ArrayList<>();
        for(String string : strings){
            doublePrices.add(Double.parseDouble(string));
        }
        System.out.println(doublePrices);
        if(Ordering.natural().reverse().isOrdered(doublePrices)){
            return true;
        }
        return false;
    }

    public void addAllSearchProductsToWishlist() {
        List<SelenideElement> wishListButtons = $$(".text-muted [data-icon='heart']");
        for(SelenideElement button : wishListButtons){
            button.click();
        }
    }
}

