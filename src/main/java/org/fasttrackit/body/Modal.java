package org.fasttrackit.body;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class Modal {
    private final SelenideElement modalTitle = $(".modal-title") ;
    private final SelenideElement closeButton = $(".close");
    private final SelenideElement usernamefield = $("#user-name" );
    private final SelenideElement passwordfield = $("#password");
    private final SelenideElement loginButton= $(".btn-primary");
    private final SelenideElement errorMesage = $( ".error");




    public String getModalTitle() {
        return modalTitle.text();
    }
    public void clickOnCloseButton() {
        this.closeButton.click();
    }

    public void clickOnUserNameField() {
       this.usernamefield.click();
    }

    public void clickOnPasswordField(){
        System.out.println("Clicked on the " + this.passwordfield );
    }

    public void clickOnTheLoginButton() {

        this.loginButton.click();
    }
    public void validateModalComponents() {

               System.out.println("7.Verify that the login button is enabled " );
    }


    public void typeInUserNameField(String userToType) {
        System.out.println("Typed in username field: " + userToType);
        this.usernamefield.click();
        this.usernamefield.sendKeys(userToType);
    }

    public void typeInPasswordField(String passwordToType) {
        System.out.println("Typed in password field: " + passwordToType);
        this.usernamefield.click();
        this.passwordfield.sendKeys(passwordToType);
    }


    public boolean validateCloseButtonIsDisplayed() {
      return this.closeButton.exists() && this.closeButton.isDisplayed();
    }

    public boolean userNameFieldIsdisplayed(){
        return  this.usernamefield.exists() && this.usernamefield.isDisplayed();

    }
    public boolean passwordFieldIsDispyed(){
        return  this.passwordfield.exists() && this.passwordfield.isDisplayed();
    }
     public boolean  loginBottonIsDisplayed(){
        return this.loginButton.exists() && this.loginButton.isDisplayed();
     }

     public String getModalMessage(){
       return this.errorMesage.text();

     }
}
