package org.fasttrackit.body;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class Header {
   private final SelenideElement shopingCartIcon = $("[href='#/cart']");
   private final SelenideElement homePageIconUrl = $(".navbar-brand");
   private final SelenideElement wishlistIconUrl = $("[href='#/wishlist']");
   private final SelenideElement greetingsMesage = $(".navbar-text");
   private final SelenideElement signInButton = $(".fa-sign-in-alt");
   private final SelenideElement signOutButton = $(".fa-sign-out-alt");

    public Header() {

           }

    public Header( String user){

            }
   //shoping cart button
   public SelenideElement getShopingCartIcon() {

        return shopingCartIcon;
    }

    public void clickOnShoppingCartIcon() {

        this.shopingCartIcon.click();
    }

   // home page button
    public SelenideElement getHomePageIconUrl() {

       return homePageIconUrl;
    }
    public  void homePageIconUrl(){
        System.out.println(" Return to the home page ");
        this.homePageIconUrl.click();
    }


    //wishlist icon
    public SelenideElement getWishlistIconUrl() {

        return wishlistIconUrl;
    }
    public void clickOnTheWishListButton() {
        this.wishlistIconUrl.click();

    }


    public SelenideElement getSignInButton() {

        return signInButton;
    }
    public void clickOnTheLoginButton() {
        this.signInButton.click();
    }
    //sign out button
    public void clickOnTheLogOutButton(){
        this.signOutButton.click();
    }
    public SelenideElement getSignOutButton() {

        return signOutButton;
    }

    public String getGreetingsMessage() {
        return greetingsMesage.text();
    }

    public void logUserOutIfLoggedIn(){
        if(this.signOutButton.isDisplayed()){
            this.signOutButton.click();
        }
    }
}
